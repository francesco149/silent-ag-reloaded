/*
	Copyright 2013 Francesco "Franc[e]sco" Noferi (francesco1149@gmail.com)

	This file is part of silent-ag-reloaded.

	silent-ag is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	silent-ag is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with silent-ag.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <memory>
#include <Windows.h>

namespace silentag
{
	class app
	{
	public:
		static app *get();
		~app();
		int run(HINSTANCE hInstance);

	protected:
		static LPCTSTR appname;
		static LPCTSTR classname;
		static LPCTSTR msgcaption;
		static std::auto_ptr<app> instance;
		HINSTANCE hInstance;
		HANDLE hThread;
		HWND hWnd;
		NOTIFYICONDATA nid;
		HMENU hPopupMenu;

		static DWORD hidemessage(LPVOID lpvParams);
		static LRESULT CALLBACK wndproc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

		app();
		bool trayinit();
		void traydestroy();
		void traychangeicon(LPCTSTR icon);
		void traymessage(LPCTSTR text, LPCTSTR icon = IDI_INFORMATION);
		void showcontextmenu();
		void startsuppress();
		void stopsuppress();
		void togglesuppress();
	};
}