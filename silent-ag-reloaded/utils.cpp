/*
	Copyright 2013 Francesco "Franc[e]sco" Noferi (francesco1149@gmail.com)

	This file is part of silent-ag-reloaded.

	silent-ag is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	silent-ag is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with silent-ag.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "utils.h"
#include <Windows.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <tchar.h>

namespace utils
{
	std::wostream& wendl(std::wostream& out) 
	{
		return out.put(L'\r').put(L'\n').flush();
	}

	void enablewin32console()
	{
		// credits to http://justcheckingonall.wordpress.com/2008/08/29/console-window-win32-app/
		HANDLE handle;
		int hCrt;
		FILE* stream;

		AllocConsole();

		// redirect stdout
		handle = GetStdHandle(STD_OUTPUT_HANDLE);
		hCrt = _open_osfhandle(reinterpret_cast<long>(handle), _O_TEXT);
		stream = _fdopen(hCrt, "w");
		setvbuf(stream, NULL, _IONBF, 1);
		*stdout = *stream;

		// redirect stdin
		handle = GetStdHandle(STD_INPUT_HANDLE);
		hCrt = _open_osfhandle(reinterpret_cast<long>(handle), _O_TEXT);
		stream = _fdopen(hCrt, "r");
		setvbuf(stream, NULL, _IONBF, 128);
		*stdin = *stream;
	}
}