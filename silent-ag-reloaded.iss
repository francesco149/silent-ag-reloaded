#define gAppName "Silent Aero Glass Reloaded"
#define gAppVerName "Silent Aero Glass Reloaded alpha-r0"
#define gAppExeName "silent-ag-reloaded.exe"
#define gAppPublisher "Francesco Franc[e]sco Noferi"
#define gAppURL ""

[Setup]
AppName={#gAppName}
AppVerName={#gAppVerName}
AppPublisher={#gAppPublisher}
AppPublisherURL={#gAppURL}
AppSupportURL={#gAppURL}
AppUpdatesURL={#gAppURL}
DefaultDirName={pf}\silent-ag-reloaded
DefaultGroupName={#gAppName}
OutputDir={#SourcePath}\..\silent-ag-reloaded-dist
OutputBaseFilename=silent-ag-reloaded-setup
SetupIconFile={#SourcePath}\silent-green.ico
Compression=lzma
SolidCompression=yes
PrivilegesRequired=admin
UsePreviousLanguage=no

[Languages]
Name: english; MessagesFile: compiler:Default.isl

[Tasks]
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: unchecked

[Files]
Source: {#SourcePath}\Release\silent-ag-reloaded.exe; DestDir: {app}; Flags: ignoreversion

[Icons]
Name: {group}\{#gAppName}; Filename: {app}\{#gAppExeName}; WorkingDir: {app}
Name: {group}\{cm:UninstallProgram,{#gAppName}}; Filename: {uninstallexe}
Name: {commondesktop}\{#gAppName}; Filename: {app}\{#gAppExeName}; Tasks: desktopicon; WorkingDir: {app}
Name: "{commonstartup}\{#gAppName}"; Filename: "{app}\{#gAppExeName}"

[Run]
Filename: {app}\{#gAppExeName}; Description: {cm:LaunchProgram,{#gAppName}}; Flags: nowait postinstall skipifsilent

[CustomMessages]
LaunchProgram=Start {#gAppName} after finishing installation